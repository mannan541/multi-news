package com.example.nazmawaqas.multinews.Service;

import org.json.JSONArray;

public interface OnJsonReturn {
	void onJsonReturn(Boolean result, JSONArray jsonArray);

}
