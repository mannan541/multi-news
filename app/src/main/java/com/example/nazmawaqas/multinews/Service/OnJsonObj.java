package com.example.nazmawaqas.multinews.Service;

import org.json.JSONArray;
import org.json.JSONObject;

public interface OnJsonObj {
    void onJsonReturn(Boolean result, JSONObject object);
}
