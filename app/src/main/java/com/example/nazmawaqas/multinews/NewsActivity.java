package com.example.nazmawaqas.multinews;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.widget.ListView;

import com.example.nazmawaqas.multinews.Service.GetAllNews;
import com.example.nazmawaqas.multinews.Service.NewsAdapter;
import com.example.nazmawaqas.multinews.Service.NewsData;
import com.example.nazmawaqas.multinews.Service.OnJsonObj;
import com.example.nazmawaqas.multinews.Service.OnJsonReturn;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class NewsActivity extends Activity implements OnJsonObj, OnJsonReturn {
    private ArrayList<NewsData> newsDataArrayList;
    ListView newsListView;
    NewsAdapter adapter;
    Context context;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);
        context = this;
        new GetAllNews(this,this).get();
        newsListView = (ListView)findViewById(R.id.listView);
    }

    @Override
    public void onJsonReturn(Boolean result, JSONObject object) {

    }

    @Override
    public void onJsonReturn(Boolean result, JSONArray jsonArray) {
        System.out.println(jsonArray.toString());
        newsDataArrayList = new ArrayList<NewsData>();
        for(int i = 0; i< jsonArray.length(); i++)
        {
            try {
                NewsData temp = new NewsData();
                JSONObject jsonObject = jsonArray.getJSONObject(i);

                temp.setId(jsonObject.get("id").toString());
                temp.setCat_id(jsonObject.get("cat_id").toString());
                temp.setTitle(jsonObject.get("title1").toString());
                temp.setTitle_2(jsonObject.get("title2").toString());
                temp.setDescription(jsonObject.get("description1").toString());
                temp.setDescription2(jsonObject.get("description2").toString());
                temp.setCreateTime(jsonObject.get("create_datetime").toString());
                temp.setImage(jsonObject.get("image1").toString());
                temp.setImage2(jsonObject.get("image2").toString());
                temp.setThumb(jsonObject.get("thumb1").toString());
                temp.setThumb2(jsonObject.get("thumb2").toString());
                temp.setLanguage(jsonObject.get("language").toString());
                temp.setIsPublished(jsonObject.get("ispublished").toString());
                newsDataArrayList.add(temp);


            }catch (Exception e){
                e.printStackTrace();
            }
        }
        adapter = new NewsAdapter(context, newsDataArrayList);
        newsListView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }
}
