package com.example.nazmawaqas.multinews.Service;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.nazmawaqas.multinews.R;

import java.util.ArrayList;

/**
 * Created by Javaid Iqbal on 5/10/2016.
 */
public class NewsAdapter extends BaseAdapter {
    ArrayList<NewsData> newsDataArrayList ;
    LayoutInflater inflater;
    Context context;

    public NewsAdapter( Context context,ArrayList<NewsData> newsDataArrayList) {
        this.newsDataArrayList = newsDataArrayList;
        inflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        this.context = context;
    }

    @Override
    public int getCount() {
        return newsDataArrayList.size();
    }

    @Override
    public NewsData getItem(int position) {
        return newsDataArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return newsDataArrayList.indexOf(newsDataArrayList.get(position));
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder= null;
        if(convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.grid_items, parent, false);
            holder.tv_order = (TextView) convertView.findViewById(R.id.tv_items);
            holder.tv_title = (TextView) convertView.findViewById(R.id.tv_items_title);
//            holder.tv_order.setText(newsDataArrayList.get(position).getDescription2());
//            holder.tv_title.setText(newsDataArrayList.get(position).getTitle_2()+": ");
            holder.tv_order.setText(newsDataArrayList.get(position).getTitle());
            holder.tv_title.setText(newsDataArrayList.get(position).getId()+": ");
        }
        return convertView;

    }
    private static class ViewHolder
    {
        public TextView tv_order;
        public TextView tv_title;

    }
}
