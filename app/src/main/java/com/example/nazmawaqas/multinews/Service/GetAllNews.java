package com.example.nazmawaqas.multinews.Service;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;

import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.StatusLine;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.methods.HttpPost;
import cz.msebera.android.httpclient.entity.StringEntity;
import cz.msebera.android.httpclient.impl.client.DefaultHttpClient;
import cz.msebera.android.httpclient.util.EntityUtils;


public class GetAllNews {
    String url = "http://sfps.com.pk/multinews/webservice/getNews.php";

    Context context;
    JSONObject jObj = null;
    boolean flag = false;
    private final String USER_AGENT = "Mozilla/5.0";
    private OnJsonReturn  listener;
    NewsData surveyData;
    public GetAllNews(Context context, OnJsonReturn  listener) {
        this.context = context;
        this.listener = listener;
    }

    public void get() {

        new AsynkT().execute();
    }

    private class AsynkT extends AsyncTask<Void, Void, Boolean> {
        private JSONArray jsonArray = null;
        @Override
        protected Boolean doInBackground(Void... params) {
            String result = "";

            try{

                URL obj = new URL(url);
                HttpURLConnection con = (HttpURLConnection) obj.openConnection();

                // optional default is GET
                con.setRequestMethod("GET");

                //add request header
                con.setRequestProperty("User-Agent", USER_AGENT);

                int responseCode = con.getResponseCode();
                System.out.println("\nSending 'GET' request to URL : " + url);
                System.out.println("Response Code : " + responseCode);

                BufferedReader in = new BufferedReader(
                        new InputStreamReader(con.getInputStream()));
                String inputLine;
                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();
                System.out.println("SERVICE"+response.toString());
                jsonArray = new JSONArray(response.toString());
                System.out.println("jObj"+jObj);
                Log.d("jObj", "SERVICE "+response.toString());
                Log.d("jObj", "jObj "+jObj);
                return true;
            } catch (UnsupportedEncodingException e1) {

                Log.d("Exception", "UnsupportedEncodingException");
                return false;
            } catch (IOException e1) {

                Log.d("IOException", e1.getMessage());
                return false;
            } catch (Exception e){
                Log.d("Exception", e.getMessage());
                return false;
            }

        }
        @Override
        protected void onPostExecute(Boolean result) {
            listener.onJsonReturn(result, jsonArray);
        }


    }
}
